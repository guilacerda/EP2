package View;

import java.awt.Dimension;
import java.awt.Font;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import Model.Produto;

public class PedidoTela extends Produto{

	private JFrame frame;
	
	public JTextField pratoField;
	public JTextField sobremesaField;
	public JTextField bebidaField;
	
	public JTextArea totalArea;
	
	public JScrollPane scroll;
	
	public JButton pratoOk;
	public JButton sobremesaOk;
	public JButton bebidaOk;
	public JButton cancelarButton;
	public JButton pagarButton;
	public JButton inserirButton;
	public JButton sairButton;
	
	@SuppressWarnings("rawtypes")
	public JComboBox pratoComboBox;
	@SuppressWarnings("rawtypes")
	public JComboBox sobremesaComboBox;
	@SuppressWarnings("rawtypes")
	public JComboBox bebidaComboBox;

	public PedidoTela(){
		initialize();
	}

	@SuppressWarnings({ "rawtypes", "unchecked", "unused" })
	private void initialize(){
		
		frame = new JFrame("Restaurante TOCOMFOME");
		frame.getContentPane().setEnabled(false);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel menuLabel = new JLabel("MENU");
		menuLabel.setHorizontalAlignment(SwingConstants.CENTER);
		menuLabel.setFont(new Font("Dialog", Font.BOLD, 18));
		menuLabel.setBounds(174, 12, 81, 27);
		frame.getContentPane().add(menuLabel);
		
		JLabel pratoLabel = new JLabel("Prato :");
		pratoLabel.setBounds(11, 67, 80, 20);
		frame.getContentPane().add(pratoLabel);
		
		JLabel itensLabel = new JLabel("Itens");
		itensLabel.setBounds(150, 40, 45, 20);
		frame.getContentPane().add(itensLabel);
		
		JLabel quantidadeLabel = new JLabel("Quantidade");
		quantidadeLabel.setBounds(248, 40, 90, 20);
		frame.getContentPane().add(quantidadeLabel);
		
		JLabel bebidaLabel = new JLabel("Bebida :");
		bebidaLabel.setBounds(11, 97, 80, 20);
		frame.getContentPane().add(bebidaLabel);
		
		JLabel sobremesaLabel = new JLabel("Sobremesa :");
		sobremesaLabel.setBounds(11, 127, 96, 20);
		frame.getContentPane().add(sobremesaLabel);
		
		pratoComboBox = new JComboBox();
		String[] pratoAux;
		try{  
	         BufferedReader read = new BufferedReader(new FileReader("doc/Prato.txt"));  
	         pratoComboBox.removeAllItems();
	         pratoComboBox.addItem("");

	         while(read.ready()){  
	        	pratoAux = read.readLine().split(";");
	            pratoComboBox.addItem(pratoAux[0]);
	         }  
	         read.close();  
	     }catch(IOException ioe){  
	         ioe.printStackTrace(); 
	      }
		pratoComboBox.setMaximumRowCount(200);
		pratoComboBox.setEditable(true);
		pratoComboBox.setBounds(112, 67, 117, 20);
		frame.getContentPane().add(pratoComboBox);
		bebidaComboBox = new JComboBox();
		
		bebidaComboBox.setMaximumRowCount(200);
		bebidaComboBox.setEditable(true);
		bebidaComboBox.setBounds(112, 97, 117, 20);
		frame.getContentPane().add(bebidaComboBox);
		String[] bebidaAux;
		try{  
			BufferedReader read = new BufferedReader(new FileReader("doc/Bebida.txt"));
			bebidaComboBox.removeAllItems();
			bebidaComboBox.addItem("");
			
			while(read.ready()){  
				bebidaAux = read.readLine().split(";");
				bebidaComboBox.addItem(bebidaAux[0]);
			}  
			read.close();  
		}catch(IOException ioe){  
			ioe.printStackTrace(); 
		}
		
		sobremesaComboBox = new JComboBox();
		sobremesaComboBox.setMaximumRowCount(200);
		sobremesaComboBox.setEditable(false);
		sobremesaComboBox.setBounds(112, 127, 117, 20);
		frame.getContentPane().add(sobremesaComboBox);
		
		String[] sobremesaAux;
		
		try{  
			BufferedReader read = new BufferedReader(new FileReader("doc/Sobremesa.txt"));  
			sobremesaComboBox.removeAllItems();
			sobremesaComboBox.addItem("");
			while(read.ready()){  
				sobremesaAux = read.readLine().split(";");
				sobremesaComboBox.addItem(sobremesaAux[0]);
			}  
			read.close();  
		}catch(IOException ioe){  
			ioe.printStackTrace(); 
		}
		
		
		pratoOk = new JButton("OK");
		pratoOk.setBounds(360, 67, 55, 20);
		frame.getContentPane().add(pratoOk);
		
		bebidaOk = new JButton("OK");
		bebidaOk.setBounds(360, 97, 55, 20);
		frame.getContentPane().add(bebidaOk);
		
		sobremesaOk = new JButton("OK");
		sobremesaOk.setBounds(360, 127, 55, 20);
		frame.getContentPane().add(sobremesaOk);
		
		pagarButton = new JButton("Pagar");
		pagarButton.setBounds(320, 190, 107, 20);
		frame.getContentPane().add(pagarButton);
		
		cancelarButton = new JButton("Cancelar");
		cancelarButton.setBounds(320, 215, 107, 20);
		frame.getContentPane().add(cancelarButton);
		
		 inserirButton = new JButton("Inserir");
		inserirButton.setBounds(320, 165, 107, 20);
		frame.getContentPane().add(inserirButton);
		
		sairButton = new JButton("Sair");
		sairButton.setBounds(320, 240, 107, 20);
		frame.getContentPane().add(sairButton);
		DefaultListModel modelo = new DefaultListModel();
				
		pratoField = new JTextField();
		pratoField.setBounds(250, 67, 80, 20);
		frame.getContentPane().add(pratoField);
		pratoField.setColumns(10);
		
		sobremesaField = new JTextField();
		sobremesaField.setColumns(10);
		sobremesaField.setBounds(250, 127, 80, 20);
		frame.getContentPane().add(sobremesaField);
		
		bebidaField = new JTextField();
		bebidaField.setColumns(10);
		bebidaField.setBounds(250, 97, 80, 20);
		frame.getContentPane().add(bebidaField);
		
		DefaultListModel<String> model = new DefaultListModel<String>();
		totalArea = new JTextArea();
		scroll = new JScrollPane(totalArea);
		scroll.setPreferredSize(new Dimension(20, 20));
		scroll.setSize(275, 92);
		scroll.setLocation(11,168);
		totalArea.setLineWrap(true);
		totalArea.setWrapStyleWord(true);
		totalArea.setEditable(false);
		frame.getContentPane().add(scroll);

		frame.setVisible(true);
		frame.setResizable(false);
	}
	
	public void setVisible(boolean b) {
		frame.setVisible(true);
	}

	public void dispose() {
		frame.dispose();
	}
}
