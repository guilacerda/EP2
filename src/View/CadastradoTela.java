package View;

import java.awt.EventQueue; 

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CadastradoTela {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CadastradoTela window = new CadastradoTela();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CadastradoTela() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 300, 100);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblDesejaCadastrarOutro = new JLabel("Deseja cadastrar outro produto?");
		lblDesejaCadastrarOutro.setBounds(25, 10, 245, 20);
		frame.getContentPane().add(lblDesejaCadastrarOutro);
		
		JButton btnSim = new JButton("Sim");
		btnSim.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
				ProdutoTela cadastrarP = new ProdutoTela();
				cadastrarP.setVisible(true);
			}
		});
		btnSim.setBounds(46, 45, 90, 25);
		frame.getContentPane().add(btnSim);
		
		JButton button = new JButton("Não");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.dispose();
				PedidoTela voltarPedido = new PedidoTela();
				voltarPedido.setVisible(true);
			}
		});
		button.setBounds(148, 45, 90, 25);
		frame.getContentPane().add(button);
	}

	public void setVisible(boolean b) {
		frame.setVisible(true);		
	}
}
