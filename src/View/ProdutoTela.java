package View;

import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class ProdutoTela {

	private JFrame frame;
	public JTextField nomeField;
	public JTextField precoField;
	public JTextField quantidadeField;
	public JTextField quantidademField;
	public JTextField tipoField;
	public JButton adicionarButton;
	public JButton voltarButton;

	public ProdutoTela() {
		initialize();
	}

	public void initialize() {
		frame = new JFrame("TOCOMFOME, QUERO MAIS");
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		//labels
		JLabel Produto_Label = new JLabel("Cadastro de Produto");
		Produto_Label.setFont(new Font("Dialog", Font.BOLD, 18));
		Produto_Label.setBounds(110, 10, 220, 20);
		frame.getContentPane().add(Produto_Label);
		
		JLabel nomeLabel = new JLabel("Nome :");
		nomeLabel.setBounds(28, 90, 70, 15);
		frame.getContentPane().add(nomeLabel);
		
		JLabel precoLabel = new JLabel("Preço (R$) :");
		precoLabel.setBounds(28, 115, 90, 15);
		frame.getContentPane().add(precoLabel);
		
		JLabel tipoLabel = new JLabel("Tipo :");
		tipoLabel.setBounds(28, 65, 70, 15);
		frame.getContentPane().add(tipoLabel);
		
		JLabel quantidadeLabel = new JLabel("Quantidade :");
		quantidadeLabel.setBounds(28, 140, 100, 15);
		frame.getContentPane().add(quantidadeLabel);
		
		JLabel quantidademLabel = new JLabel("Quantidade Mínima :");
		quantidademLabel.setBounds(28, 165, 165, 15);
		frame.getContentPane().add(quantidademLabel);
		
		//Fields
		nomeField = new JTextField();
		nomeField.setBounds(194, 90, 200, 20);
		frame.getContentPane().add(nomeField);
		nomeField.setColumns(10);
		
		precoField = new JTextField();
		precoField.setColumns(10);
		precoField.setBounds(194, 115, 200, 20);
		frame.getContentPane().add(precoField);
		
		quantidadeField = new JTextField();
		quantidadeField.setColumns(10);
		quantidadeField.setBounds(194, 140, 200, 20);
		frame.getContentPane().add(quantidadeField);

		tipoField = new JTextField();
		tipoField.setColumns(10);
		tipoField.setBounds(194, 65, 200, 20);
		frame.getContentPane().add(tipoField);
		
		quantidademField = new JTextField();
		quantidademField.setColumns(10);
		quantidademField.setBounds(194, 165, 200, 20);
		frame.getContentPane().add(quantidademField);
		
		
		//Buttons
		adicionarButton = new JButton("Adicionar");
		adicionarButton.setBounds(105, 203, 117, 25);
		frame.getContentPane().add(adicionarButton);
		
		voltarButton = new JButton("Voltar");
		voltarButton.setBounds(230, 203, 117, 25);
		frame.getContentPane().add(voltarButton);
		
		frame.setVisible(true);
		frame.setResizable(false);
	}

	public void setVisible(boolean b) {
		frame.setVisible(true);
		JOptionPane.showMessageDialog(null, "Tipos : Prato, Bebida e Sobremesa");
		
	}

	public void dispose() {
		frame.dispose();
	}
}
