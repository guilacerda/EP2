package View;


import java.awt.Font; 
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;


public class LoginTela{
	
	private JFrame frame;
	private JLabel usuarioLabel;
	public JButton btnEntrar;
	public JButton btnSair;
	public JButton btnCadastrar;
	public JPasswordField Senha_Login;
	public JTextField Usuario_Field;

	public LoginTela() {
		initialize();
	}

	public void initialize() {
		frame = new JFrame("TOCOMFOME, QUERO MAIS");
		frame.setBounds(100, 100, 450, 280);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		//Labels
		usuarioLabel = new JLabel("Usuário :");
		usuarioLabel.setBounds(90, 80, 79, 24);
		frame.getContentPane().add(usuarioLabel);
				
		JLabel senhaLabel = new JLabel("Senha :");
		senhaLabel.setBounds(90, 120, 79, 24);
		frame.getContentPane().add(senhaLabel);
		
		JLabel loginLabel = new JLabel("LOGIN");
		loginLabel.setHorizontalAlignment(SwingConstants.CENTER);
		loginLabel.setFont(new Font("Dialog", Font.BOLD, 18));
		loginLabel.setBounds(170, 20, 100, 20);
		frame.getContentPane().add(loginLabel);
		
		//Fields
		Usuario_Field = new JTextField();
		Usuario_Field.setBounds(192, 80, 148, 21);
		frame.getContentPane().add(Usuario_Field);
		Usuario_Field.setColumns(10);
		
		Senha_Login = new JPasswordField();
		Senha_Login.setBounds(192, 120, 148, 21);
		frame.getContentPane().add(Senha_Login);
		
		//Buttons
		btnSair = new JButton("Sair");
		btnSair.setBounds(287, 180, 112, 25);
		frame.getContentPane().add(btnSair);
		
		btnEntrar = new JButton("Entrar");
		btnEntrar.setBounds(39, 180, 112, 25);
		frame.getContentPane().add(btnEntrar);
		
		btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.setBounds(163, 180, 112, 25);
		frame.getContentPane().add(btnCadastrar);
		
		frame.setVisible(true);
		frame.setResizable(false);
	}

	public void dispose() {
		frame.dispose();
	}
}
