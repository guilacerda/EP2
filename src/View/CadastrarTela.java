package View;

import java.awt.Font; 

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class CadastrarTela {

	public JFrame frame;
	public JPasswordField Senha_Field;
	public JPasswordField RepitaSenha_Field;
	public JButton btnFinalizar;
	public JButton btnVoltar;
	public JTextField Usuario_Field;
	public JTextField Nome_Field;
	private JLabel lblCadastroDeFuncionrio;
	private JLabel RepitaSenha_Label;

	public CadastrarTela() {
		initialize();
	}

	public void initialize() {
		frame = new JFrame("TOCOMFOME, QUERO MAIS");
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		//Labels
		lblCadastroDeFuncionrio = new JLabel("CADASTRO DE FUNCIONÁRIO");
		lblCadastroDeFuncionrio.setHorizontalAlignment(SwingConstants.CENTER);
		lblCadastroDeFuncionrio.setFont(new Font("Dialog", Font.BOLD, 18));
		lblCadastroDeFuncionrio.setBounds(75, 10, 310, 50);
		frame.getContentPane().add(lblCadastroDeFuncionrio);
		
		JLabel Nome_Label = new JLabel("Nome* :");
		Nome_Label.setBounds(70, 73, 120, 25);
		frame.getContentPane().add(Nome_Label);
		
		RepitaSenha_Label = new JLabel("Repita a senha* :");
		RepitaSenha_Label.setBounds(70, 175, 130, 25);
		frame.getContentPane().add(RepitaSenha_Label);
		
		JLabel Usuario_Label = new JLabel("Novo usuário* :");
		Usuario_Label.setBounds(70, 107, 120, 25);
		frame.getContentPane().add(Usuario_Label);
		
		JLabel Senha_Label = new JLabel("Nova senha* :");
		Senha_Label.setBounds(70, 141, 120, 25);
		frame.getContentPane().add(Senha_Label);
		
		//Field
		RepitaSenha_Field = new JPasswordField();
		RepitaSenha_Field.setBounds(230, 175, 140, 25);
		frame.getContentPane().add(RepitaSenha_Field);
		
		Senha_Field = new JPasswordField();
		Senha_Field.setBounds(230, 141, 140, 25);
		frame.getContentPane().add(Senha_Field);
		
		Usuario_Field = new JTextField();
		Usuario_Field.setBounds(230, 107, 140, 25);
		frame.getContentPane().add(Usuario_Field);
		Usuario_Field.setColumns(10);
		
		Nome_Field = new JTextField();
		Nome_Field.setColumns(10);
		Nome_Field.setBounds(230, 73, 140, 25);
		frame.getContentPane().add(Nome_Field);
		
		//Buttons
		btnFinalizar = new JButton("Finalizar");
		btnFinalizar.setBounds(120, 225, 95, 25);
		frame.getContentPane().add(btnFinalizar);
				
		btnVoltar = new JButton("Voltar");
		btnVoltar.setBounds(230, 225, 95, 25);
		frame.getContentPane().add(btnVoltar);	
		
		setVisible(true);
		frame.setResizable(false);
	}
	
	public void setVisible(boolean b) {
		frame.setVisible(true);
	}

	public void dispose() {
		frame.dispose();	
	}
}