package View;

import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class oobservacaoTela {

	private JFrame frame;
	public JButton btnSim;
	public JButton btnNao;

	public oobservacaoTela() {
		initialize();
	}

	public void initialize() {
		frame = new JFrame("TOCOMFOME, QUERO MAIS");
		frame.setBounds(100, 100, 350, 150);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblDesej = new JLabel("Deseja fazer alguma observação ?");
		lblDesej.setFont(new Font("Dialog", Font.BOLD, 14));
		lblDesej.setBounds(36, 32, 302, 20);
		frame.getContentPane().add(lblDesej);
		
		btnSim = new JButton("Sim");
		btnSim.setBounds(61, 75, 100, 25);
		frame.getContentPane().add(btnSim);
		
		btnNao = new JButton("Não");
		btnNao.setBounds(173, 75, 100, 25);
		frame.getContentPane().add(btnNao);
		
		frame.setVisible(true);
	}



	public void dispose() {
		frame.dispose();
	}

}
