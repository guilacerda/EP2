package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import Model.Cliente;
import View.PagamentoTela;

public class pagarController extends Cliente {
	public PagamentoTela pagamentoView;
	public Cliente cliente;
	
	public pagarController(){
		pagamentoView = new PagamentoTela();
		cliente = new Cliente();
		
		cadastrarController(pagamentoView.btnCadastrar);
		finalizarController(pagamentoView.btnFinalizar);
		cancelarController(pagamentoView.btnCancelar);
	}
	
	public void cadastrarController(JButton btnCadastrar){
		btnCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					if(!pagamentoView.nomeField.getText().equals("")){
						
						cadastrar();
						pagamentoView.nomeField.setText("");
						pagamentoView.cpfField.setText("");
					}else{
						JOptionPane.showMessageDialog(null,"Cadastro inválido!");
					}
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
	}
	
	public void finalizarController(JButton btnFinalizar){
		btnFinalizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pagamentoView.dispose();
				new oobservacaoController();
			}
		});
	}
	
	public void cancelarController(JButton btnCancelar){
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pagamentoView.dispose();
				//PedidoTela voltarPedido = new PedidoTela();
				//voltarPedido.setVisible(true);
			}
		});
	}
	
	public void cadastrar() throws IOException{		
		
		BufferedWriter buffWrite;
		buffWrite = new BufferedWriter(new FileWriter("doc/Cliente.txt", true));
		
		setName(pagamentoView.nomeField.getText());
		setCpf(pagamentoView.cpfField.getText());
		
		String auxiliar = getName() + ";" + getCpf() + "\n";
		buffWrite.append(auxiliar);
		buffWrite.close();
	}
}
