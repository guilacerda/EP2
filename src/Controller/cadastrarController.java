package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import Model.Funcionario;
import View.CadastrarTela;

public class cadastrarController extends Funcionario{
	
	public CadastrarTela cadastrarF;
	public Funcionario funcionarioModel;
	
	public cadastrarController(){
		cadastrarF = new CadastrarTela();
		funcionarioModel = new Funcionario();
		
		finalizarController(cadastrarF.btnFinalizar);
		voltarController(cadastrarF.btnVoltar);
	}
	
	public void finalizarController(JButton btnFinalizar){
		btnFinalizar.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				if(!cadastrarF.Nome_Field.getText().equals("") && !cadastrarF.Usuario_Field.getText().equals("")){
					if(!cadastrarF.Senha_Field.getText().equals("") && !cadastrarF.RepitaSenha_Field.getText().equals("")){
						if(cadastrarF.Senha_Field.getText().equals(cadastrarF.RepitaSenha_Field.getText())){
							try{
								BufferedWriter buff;
								buff = new BufferedWriter(new FileWriter("doc/funcionarios.txt", true));
								
								setLogin(cadastrarF.Usuario_Field.getText());
								setPassword(cadastrarF.Senha_Field.getText());
																
								String auxiliar = getlogin() + ";" + getPassword() + "\n";
								
								buff.append(auxiliar);
								buff.close();
								
								cadastrarF.dispose();
								new loginController();
								
							}catch(IOException e1){
								e1.printStackTrace();
							}
						}else{
							cadastrarF.RepitaSenha_Field.setText("");
							JOptionPane.showMessageDialog(null, "Senha de confirmação incorreta!");
						}
					}else{
						JOptionPane.showMessageDialog(null, "Campos não cadastrados!");
				     }		
				}else{
					JOptionPane.showMessageDialog(null, "Campos não cadastrados!");
				}
			}
		});
	}
	
	public void voltarController(JButton btnVoltar){
		btnVoltar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cadastrarF.dispose();
				new loginController();
			}
		});
	}
	
@SuppressWarnings("deprecation")
public void cadastrar() throws IOException{		
				
		BufferedWriter buffWrite;
		buffWrite = new BufferedWriter(new FileWriter("doc/funcionarios.txt", true));
		
		setLogin(cadastrarF.Usuario_Field.getText());
		setPassword(cadastrarF.Senha_Field.getText());
		
		String auxiliar = getlogin() + ";" + getPassword() + "\n";
		buffWrite.append(auxiliar);
		buffWrite.close();
	}

}
