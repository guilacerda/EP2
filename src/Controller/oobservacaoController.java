package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import View.oobservacaoTela;

public class oobservacaoController {
	public oobservacaoTela obsView;
	
	public oobservacaoController(){
		obsView = new oobservacaoTela();
		
		simController(obsView.btnSim);
		naoController(obsView.btnNao);
	}
	
	public void simController(JButton btnSim){
		btnSim.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				obsView.dispose();
				new observacaoController();
			}
		});
	}
	
	public void naoController(JButton btnNao){
		btnNao.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				obsView.dispose();
				new pedidoController();
			}
		});
	}
}
