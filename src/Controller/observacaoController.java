package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JButton;

import Model.Observacao;
import View.ObservacaoTela;

public class observacaoController extends Observacao{
	public ObservacaoTela observacaoView;
	public Observacao obs;
	
	public observacaoController(){
		observacaoView = new ObservacaoTela();
		obs = new Observacao();
		
		concluirController(observacaoView.btnConcluir);
		cancelarController(observacaoView.btnCancelar);
	}
	
	public void concluirController(JButton btnConcluir){
		btnConcluir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					setObs(observacaoView.observacaoArea.getText());	
					
					String auxiliar = "\n----------------------------\n" + "Observação do cliente : \n" +  getObs() + "\n";

					BufferedWriter buffWrite;
					buffWrite = new BufferedWriter(new FileWriter("doc/Observacao.txt", true));
					buffWrite.append(auxiliar);
					
					buffWrite.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				observacaoView.dispose();
				new pedidoController();
			}
		});
	}
	
	public void cancelarController(JButton btnCancelar){
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				observacaoView.dispose();
				new pedidoController();
			}
		});
	}
}
