package Model;

public class Pagamento {

	private boolean cartao;
	private boolean dinheiro;
		
	public boolean getDinheiro() {
		return dinheiro;
	}
	public void setDinheiro(boolean dinheiro) {
		this.dinheiro = dinheiro;
	}
	
	public boolean getCartao() {
		return cartao;
	}
	public void setCartao(boolean cartao) {
		this.cartao = cartao;
	}

}
