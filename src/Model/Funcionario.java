package Model;

public class Funcionario extends Pessoa {

	private String login;
	private String password;
		
	public Funcionario(){
		
	}
	
	public String getlogin(){
		return login;
	}
	
	public String getPassword(){
		return password;
	}
	
	public void setPassword(String password){
		this.password = password;
	}
	
	public void setLogin(String login){
		this.login = login;
	}
}
