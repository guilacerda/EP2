# EP2 - OO (UnB - Gama)

Este projeto consiste em uma aplicação desktop para um restaurante, utilizando a técnologia Java Swing.

## Como Compilar e Executar

Para compilar e executar o programa em um sistema operacional Linux, siga as seguintes instruções:

* Abra o eclipse;
* Encontre o diretório raiz do projeto;
* Compile e Execute:
	**CTRL + F11**

* Outras IDE's
* Encontre o diretório raiz do projeto;
* Compile e Execute:
    **Botões de compilar e executar**
    
## Funcionamento

O funcionamento do programa está descrito a seguir

* Na tela de login, o programa pedirá um usuário e senha cadastrada
* Caso o funcionário não tenha se cadastrado, poderá se cadastrar no botão "Cadastrar"
* Clicando no botão "ENTRAR" iremos para a interface principal, a qual realiza os pedidos
* Usando o botão "INSERIR" podemos cadastrar novos produtos
* Botão "Cancelar" cancela o pedido do usuário
* Clicando no botão "PAGAR" avançamos da tela de pedido para a tela de pagamento
* Nela podemos cadastrar cliente e selecionar o tipo de pagamento
* Se o cliente avançar na compra, teremos a opção de observação (Sim ou não)
* Logo após a observação, voltamos para o pedido para atender outros clientes
* Os botões "Sair" o leva para a página de login e para o fechamento do programa.

